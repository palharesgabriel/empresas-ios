//
//  Request.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import Foundation

enum StatusError {
    case urlError(message: String)
    case httpResponseCastError(message: String)
    case withoutConnection(message: String)
    case invalidCredentials(message: String)
    case APIError(error: String)
    case undefinedError(message: String)
}

class Request {
    
    static let server = "https://empresas.ioasys.com.br/api/v1/"
    
    static func run(endPoint: EmpresasAPI, completion: @escaping (HeadersAuth?, StatusError?)->()) {
        let urlString = server + endPoint.path
                    
        guard let url = URL(string: urlString) else {
            DispatchQueue.main.async {
                completion(nil, StatusError.urlError(message: "Erro ao montar a URL a partir de uma String"))
            }
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = endPoint.httpMethod
        if let data = endPoint.httpBody {
            request.httpBody = data
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
                    
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            
            guard let httpURLResponse = response as? HTTPURLResponse else {
                DispatchQueue.main.async {
                    completion(nil, StatusError.httpResponseCastError(message: "Não foi possível converter a response para HTTPURLResponse!"))
                }
                return
            }
            
            switch httpURLResponse.statusCode {
                case 200:
                    let headers = HeadersAuth(response: httpURLResponse)
                    DispatchQueue.main.async {
                        completion(headers, nil)
                    }
                case 401:
                    completion(nil, StatusError.invalidCredentials(message: "Credenciais inválidas, tente novamente!"))
                case 408:
                    completion(nil, StatusError.withoutConnection(message: "Não foi possível estabelecer conexão com a API!"))
                default:
                    completion(nil, StatusError.undefinedError(message: "Erro desconhecido"))
            }
           
            if error != nil {
                DispatchQueue.main.async {
                    completion(nil, StatusError.APIError(error: "Request falhou com o erro: \(error.debugDescription)"))
                }
            }

        }).resume()
    }
    
    
    static func run(endPoint: EmpresasAPI, headers: HeadersAuth, completion: @escaping (Result?)->()) {
        let urlString = server + endPoint.path
        
        guard let url = URL(string: urlString) else {
            DispatchQueue.main.async {
                completion(nil)
            }
            return
        }
        
        guard let token = headers.accessToken, let client = headers.client, let uid = headers.uid else {
            DispatchQueue.main.async {
                completion(nil)
            }
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = endPoint.httpMethod
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(token, forHTTPHeaderField: "access-token")
        request.addValue(client, forHTTPHeaderField: "client")
        request.addValue(uid, forHTTPHeaderField: "uid")
        
        if let data = endPoint.httpBody {
            request.httpBody = data
        }
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let responseData = data {
                do {
                    let jsonObject = try JSONDecoder().decode(Result.self, from: responseData)
                    DispatchQueue.main.async {
                        completion(jsonObject)
                        debugPrint(jsonObject)
                    }
                } catch let jsonError {
                    DispatchQueue.main.async {
                        print("Catch: \(jsonError)")
                        completion(nil)
                    }
                }
            }
            
            if error != nil {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
            
        }).resume()
    }
    
    
}


    

