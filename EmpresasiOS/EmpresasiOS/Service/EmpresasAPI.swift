//
//  EmpresasAPI.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import Foundation

enum EmpresasAPI {
    case authenticateUser(user: UserAuth)
    case getEnterprise(name: String)
}

extension EmpresasAPI {
    
    var path: String {
        switch self {
        case .authenticateUser:
            return "users/auth/sign_in"
            
        case .getEnterprise(let name):
            return "enterprises?name=\(name)"
        }
    }
    
    var httpMethod: String {
        switch self {
        case .authenticateUser:
            return "POST"
        case .getEnterprise:
            return "GET"
        }
    }
    
    var httpBody: Data? {
        switch self {
        case .authenticateUser(let user):
            do {
                let jsonData = try JSONEncoder().encode(user)
                return jsonData
                
            } catch let jsonError {
                print("Catch: \(jsonError)")
                return nil
            }
        case .getEnterprise:
            return nil
        }
    }
    
}

