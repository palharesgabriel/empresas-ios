//
//  LoginViewController.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 27/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    private var loginView: LoginView!
    static let notificationLogin = NSNotification.Name(rawValue: "Login")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginView = LoginView(delegate: self)
        setupView()
        view.backgroundColor = .white
    }
    
    private func authenticate(_ user: UserAuth) {
        let loadingView = LoadingView()
        loadingView.beSubview(view)
        Request.run(endPoint: EmpresasAPI.authenticateUser(user: user)) { (headers, statusError) in
            DispatchQueue.main.async {
                loadingView.removeFromSuperview()
            }
            
            if let customHeaders = headers {
                DispatchQueue.main.async {
                    self.loginView.validateTextFieldCredentials(loginSuccess: true)
                }
                NotificationCenter.default.post(name: LoginViewController.notificationLogin, object: customHeaders)
            }
            
            if let error = statusError {
                switch error {
                case .invalidCredentials:
                    DispatchQueue.main.async {
                        self.loginView.validateTextFieldCredentials(loginSuccess: false)
                    }
                default:
                    debugPrint(error)
                    let alert = UIAlertController(title: "Tente novamente mais tarde", message: "Estamos com problemas no servidor", preferredStyle: .alert)
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
}

extension LoginViewController: ConfigurableView {
    func buildViewHierarchy() {
        view.addSubviews([loginView])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            loginView.topAnchor.constraint(equalTo: view.topAnchor),
            loginView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            loginView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            loginView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension LoginViewController: LoginViewDelegate {
    func loginRequested(userEmail: String, userPassword: String) {
        let user = UserAuth(email: userEmail, password: userPassword)
        authenticate(user)
    }
}

