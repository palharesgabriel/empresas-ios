//
//  FormView.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

protocol FormViewDelegate: class {
    func enterButtonDidPressed(userEmail: String, userPassword: String)
}

class FormView: UIView {
    
    weak var delegate: FormViewDelegate?
    
    private let emailLabel = BaseTextFieldTitleLabel(text: "Email")
    private let passwordLabel = BaseTextFieldTitleLabel(text: "Senha")
    
    private let emailTextField = BaseTextField(frame: .zero)
    private let passwordTextField = BaseTextField(frame: .zero)
    
    private let loginButton = BaseButton(title: "ENTRAR")
    
    private let loginErrorLabel: UILabel = {
        let label = UILabel()
        label.textColor = .red
        label.text = "Credenciais Incorretas"
        label.textAlignment = .right
        label.isHidden = true
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    convenience init(delegate: FormViewDelegate) {
        self.init(frame: .zero)
        self.delegate = delegate
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        emailTextField.text = "testeapple@ioasys.com.br"
        passwordTextField.text = "12341234"
        
        loginButton.addTarget(self, action: #selector(enterButtonDidPressed), for: .touchDown)
    
        backgroundColor = .white
        setupView()
        setupTextFields()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupTextFields() {
        emailTextField.addLeftPadding()
        passwordTextField.addLeftPadding()
        passwordTextField.isSecureTextEntry = true
        emailTextField.setWarningBorder(width: 0)
        passwordTextField.setWarningBorder(width: 0)
        let button = passwordTextField.addEyeButton()
        button.addTarget(self, action: #selector(eyeToggleDidTapped), for: .touchDown)
    }
    
    private func invalidateTextFields() {
        emailTextField.setWarningBorder(width: 1)
        passwordTextField.setWarningBorder(width: 1)
    }
    
    func validateTextFieldCredentials(loginSuccess: Bool) {
        if loginSuccess {
            loginErrorLabel.isHidden = true
            setupTextFields()
            
        } else {
            invalidateTextFields()
            loginErrorLabel.isHidden = false
        }
    }
    
    @objc func eyeToggleDidTapped() {
        passwordTextField.isSecureTextEntry.toggle()
    }
    
    @objc func enterButtonDidPressed() {
        if let email = emailTextField.text, let password = passwordTextField.text {
            delegate?.enterButtonDidPressed(userEmail: email, userPassword: password)
        }
    }
}

extension FormView: ConfigurableView {
    func buildViewHierarchy() {
        addSubviews([emailLabel, passwordLabel, emailTextField, passwordTextField, loginButton, loginErrorLabel])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            emailLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            emailLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            emailLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            emailLabel.heightAnchor.constraint(equalToConstant: 18),
            
            emailTextField.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: 4),
            emailTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            emailTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            emailTextField.heightAnchor.constraint(equalToConstant: 48),
            
            passwordLabel.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 16),
            passwordLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            passwordLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            passwordLabel.heightAnchor.constraint(equalToConstant: 18),
            
            passwordTextField.topAnchor.constraint(equalTo: passwordLabel.bottomAnchor, constant: 4),
            passwordTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            passwordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            passwordTextField.heightAnchor.constraint(equalToConstant: 48),
            
            loginErrorLabel.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 4),
            loginErrorLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            loginErrorLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            loginErrorLabel.heightAnchor.constraint(equalToConstant: 16),
            
            loginButton.topAnchor.constraint(equalTo: loginErrorLabel.bottomAnchor, constant: 24),
            loginButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            loginButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
            loginButton.heightAnchor.constraint(equalToConstant: 48)
        ])
    }
}
