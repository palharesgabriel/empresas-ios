//
//  RoundedHeaderView.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 27/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class RoundedHeaderView: GradientView {
    
    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        if let image = UIImage(named: "JustLogo") {
            imageView.image = image
        }
        return imageView
    }()
    
    private let welcomeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 20)
        label.text = "Seja bem vindo ao empresas!"
        return label
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addBottomRoundedEdge(desiredCurve: 2.0)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        startColor = .purpleMainColor
        endColor = .pinkMainColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension RoundedHeaderView: ConfigurableView {
    func buildViewHierarchy() {
        addSubviews([iconImageView, welcomeLabel])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            iconImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -16),
            iconImageView.heightAnchor.constraint(equalToConstant: 30),
            iconImageView.widthAnchor.constraint(equalToConstant: 40),

            welcomeLabel.centerXAnchor.constraint(equalTo: iconImageView.centerXAnchor),
            welcomeLabel.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: 16),
            welcomeLabel.heightAnchor.constraint(equalToConstant: 30),
            welcomeLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            welcomeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
}
