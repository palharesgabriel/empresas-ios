//
//  LoginView.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

protocol LoginViewDelegate: class {
    func loginRequested(userEmail: String, userPassword: String)
}

class LoginView: UIView {
    
    weak var delegate: LoginViewDelegate?
    
    private var roundedView: RoundedHeaderView!
    private var formView: FormView!
    
    convenience init(delegate: LoginViewDelegate) {
        self.init(frame: .zero)
        self.delegate = delegate
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        roundedView = RoundedHeaderView(frame: .zero)
        formView = FormView(delegate: self)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func validateTextFieldCredentials(loginSuccess: Bool) {
        formView.validateTextFieldCredentials(loginSuccess: loginSuccess)
    }
    
}

extension LoginView: ConfigurableView {
    func buildViewHierarchy() {
        addSubviews([roundedView, formView])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            roundedView.topAnchor.constraint(equalTo: topAnchor),
            roundedView.leadingAnchor.constraint(equalTo: leadingAnchor),
            roundedView.trailingAnchor.constraint(equalTo: trailingAnchor),
            roundedView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.35),
            
            formView.topAnchor.constraint(equalTo: roundedView.bottomAnchor, constant: 16),
            formView.leadingAnchor.constraint(equalTo: leadingAnchor),
            formView.trailingAnchor.constraint(equalTo: trailingAnchor),
            formView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}

extension LoginView: FormViewDelegate {
    func enterButtonDidPressed(userEmail: String, userPassword: String) {
        delegate?.loginRequested(userEmail: userEmail, userPassword: userPassword)
    }
}
