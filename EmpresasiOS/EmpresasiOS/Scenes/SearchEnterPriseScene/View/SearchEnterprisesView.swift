//
//  SearchEnterprisesView.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 29/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

protocol SearchEnterprisesViewDelegate: class {
    func searchTextFieldDidUpdate(with text: String)
}

class SearchEnterprisesView: UIView {
    
    let searchTextField = SearchTextField(frame: .zero)
    let gradientView = GradientView(startColor: .purpleMainColor, endColor: .pinkMainColor)
    
    weak var delegate: SearchEnterprisesViewDelegate?
    
    private let resultFoundLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .systemGray
        return label
    }()
    
    let resultNotFoundLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.text = "Nenhum resultado encontrado"
        label.textColor = .systemGray
        label.textAlignment = .center
        return label
    }()
    
    let enterprisesTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(EnterpriseTableViewCell.self, forCellReuseIdentifier: EnterpriseTableViewCell.reuseIdentifier)
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupTableViewConstraints()
        searchTextField.addTarget(self, action: #selector(searchTextFieldDidUpdate), for: .editingChanged)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(tableViewDelegate: UITableViewDelegate, tableViewdataSource: UITableViewDataSource, enterpisesViewDelegate: SearchEnterprisesViewDelegate) {
        self.init(frame: .zero)
        enterprisesTableView.delegate = tableViewDelegate
        enterprisesTableView.dataSource = tableViewdataSource
        self.delegate = enterpisesViewDelegate
    }
    
    func setResultsLabel(with text: String) {
        resultFoundLabel.text = "\(text) resultado(s) encontrados"
    }
    
    func setLabelVisibility(_ bool: Bool) {
        resultFoundLabel.isHidden = bool
    }
    
    private func setupTableViewConstraints() {
        addSubviews([resultFoundLabel, enterprisesTableView])
        NSLayoutConstraint.activate([
            resultFoundLabel.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 16),
            resultFoundLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            resultFoundLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            resultFoundLabel.heightAnchor.constraint(equalToConstant: 24),
            
            enterprisesTableView.topAnchor.constraint(equalTo: resultFoundLabel.bottomAnchor, constant: 16),
            enterprisesTableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            enterprisesTableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            enterprisesTableView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
        ])
    }
    
    @objc func searchTextFieldDidUpdate() {
        delegate?.searchTextFieldDidUpdate(with: searchTextField.text!)
    }
    
}

extension SearchEnterprisesView: ConfigurableView {
    func buildViewHierarchy() {
        gradientView.addSubviews([searchTextField])
        addSubviews([gradientView])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            gradientView.topAnchor.constraint(equalTo: topAnchor),
            gradientView.leadingAnchor.constraint(equalTo: leadingAnchor),
            gradientView.trailingAnchor.constraint(equalTo: trailingAnchor),
            gradientView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.25),
            
            searchTextField.bottomAnchor.constraint(equalTo: gradientView.bottomAnchor, constant: 24),
            searchTextField.leadingAnchor.constraint(equalTo: gradientView.leadingAnchor, constant: 16),
            searchTextField.trailingAnchor.constraint(equalTo: gradientView.trailingAnchor, constant: -16),
            searchTextField.heightAnchor.constraint(equalToConstant: 48),
            
        ])
    }
}
