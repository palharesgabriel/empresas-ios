//
//  EnterpriseTableViewCell.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 29/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {
    
    private let enterpriseTitleView = EnterpriseTitleView(frame: .zero)
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let margins = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        contentView.frame = contentView.frame.inset(by: margins)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        selectionStyle = .none
        backgroundColor = .clear
        contentView.layer.cornerRadius = 4
        contentView.clipsToBounds = true
        contentView.layer.masksToBounds = true
    }
    
    func setContent(enterprise: Enterprise) {
        enterpriseTitleView.setContent(enterprise: enterprise)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension EnterpriseTableViewCell: ConfigurableView {
    func buildViewHierarchy() {
        contentView.addSubviews([enterpriseTitleView])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            enterpriseTitleView.topAnchor.constraint(equalTo: contentView.topAnchor),
            enterpriseTitleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            enterpriseTitleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            enterpriseTitleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }
}
