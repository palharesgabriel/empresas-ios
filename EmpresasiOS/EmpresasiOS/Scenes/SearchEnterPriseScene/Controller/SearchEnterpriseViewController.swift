//
//  SearchEnterpriseViewController.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 29/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class SearchEnterpriseViewController: UIViewController {
    
    var header: HeadersAuth?
    var searchEnterprisesView: SearchEnterprisesView!
    var enterprises: [Enterprise]?
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchEnterprisesView = SearchEnterprisesView(tableViewDelegate: self, tableViewdataSource: self, enterpisesViewDelegate: self)
        searchEnterprisesView.searchTextField.delegate = self
        view.backgroundColor = .white
        setupView()
        setupNavigationBackButtons()
    }
    
    private func setupNavigationBackButtons() {
        if let arrowImage = UIImage(named: "arrow") {
            navigationController?.navigationBar.backIndicatorImage = arrowImage
            navigationController?.navigationBar.backIndicatorTransitionMaskImage = arrowImage
        }
        navigationItem.leftItemsSupplementBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    private func fetchEnterprises(name: String) {
        if let headers = self.header {
            Request.run(endPoint: EmpresasAPI.getEnterprise(name: name), headers: headers) { (requestResult: Result?) in
                
                if let result = requestResult {
                    DispatchQueue.main.async {
                        self.updateUI(with: result.enterprises)
                    }
                }
            }
        }
    }
    
    private func updateUI(with result: [Enterprise]) {
        enterprises = result
        searchEnterprisesView.enterprisesTableView.reloadData()
        searchEnterprisesView.setResultsLabel(with: String(result.count))
        searchEnterprisesView.setLabelVisibility(result.isEmpty)
        if result.isEmpty {
            searchEnterprisesView.enterprisesTableView.backgroundView = searchEnterprisesView.resultNotFoundLabel
        }
    }

}

extension SearchEnterpriseViewController: ConfigurableView {
    func buildViewHierarchy() {
        view.addSubviews([searchEnterprisesView])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            searchEnterprisesView.topAnchor.constraint(equalTo: view.topAnchor),
            searchEnterprisesView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            searchEnterprisesView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            searchEnterprisesView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
}

extension SearchEnterpriseViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let enterprises = self.enterprises else { return }
        let detailController = EnterprisesDetailViewController()
        detailController.enterprise = enterprises[indexPath.row]
        navigationController?.pushViewController(detailController, animated: true)
    }
}

extension SearchEnterpriseViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let enterprises = self.enterprises {
            return enterprises.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = searchEnterprisesView.enterprisesTableView.dequeueReusableCell(withIdentifier: EnterpriseTableViewCell.reuseIdentifier, for: indexPath) as? EnterpriseTableViewCell
        if let enterprises = self.enterprises {
            cell?.setContent(enterprise: enterprises[indexPath.row])
        }
        
        return cell ?? UITableViewCell()
    }
}

extension SearchEnterpriseViewController: SearchEnterprisesViewDelegate {
    func searchTextFieldDidUpdate(with text: String) {
        if !text.isEmpty {
             fetchEnterprises(name: text)
        }
    }
}

extension SearchEnterpriseViewController: UITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        updateUI(with: [Enterprise]())
        textField.resignFirstResponder()
        return true
    }
}

