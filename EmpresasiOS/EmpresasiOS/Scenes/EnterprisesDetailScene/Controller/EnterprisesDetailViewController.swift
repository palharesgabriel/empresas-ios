//
//  EnterprisesDetailViewController.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 20/05/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class EnterprisesDetailViewController: UIViewController {
    
    var enterprise: Enterprise?
    let enterpriseDetailView = EnterprisesDetailView(frame: .zero)
    var customNavBar: CustomNavBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        customNavBar = CustomNavBar(delegate: self)
        
        if let enterprise = self.enterprise {
            setContent(with: enterprise)
        }
        setupView()
    }
    
    private func setContent(with enterprise: Enterprise) {
        title = enterprise.enterpriseName
        enterpriseDetailView.setViewContent(enterprise: enterprise)
        customNavBar.setTitle(with: enterprise.enterpriseName)
    }
    
}

extension EnterprisesDetailViewController: ConfigurableView {
    func buildViewHierarchy() {
        view.addSubviews([customNavBar, enterpriseDetailView])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            customNavBar.topAnchor.constraint(equalTo: view.topAnchor),
            customNavBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            customNavBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            customNavBar.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.15),
            
            enterpriseDetailView.topAnchor.constraint(equalTo: customNavBar.bottomAnchor),
            enterpriseDetailView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            enterpriseDetailView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            enterpriseDetailView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
}

extension EnterprisesDetailViewController: CustomNavBarDelegate {
    func backButtonDidPressed() {
        self.navigationController?.popViewController(animated: true)
    }
}
