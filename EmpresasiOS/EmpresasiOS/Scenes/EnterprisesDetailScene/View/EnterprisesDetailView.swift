//
//  EnterprisesDetailView.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 21/05/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class EnterprisesDetailView: UIView {
    
    private let enterpriseTitleView = EnterpriseTitleView(frame: .zero)
    
    private let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        return scrollView
    }()
    
    private let descriptionTextView: UITextView = {
        let textView = UITextView()
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = UIFont.systemFont(ofSize: 18)
        textView.text = "Sem descrição"
        return textView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        enterpriseTitleView.backgroundColor = .enterpriseCellBlueColor
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setViewContent(enterprise: Enterprise) {
        enterpriseTitleView.setContent(enterprise: enterprise)
        descriptionTextView.text = enterprise.description
    }
    
}

extension EnterprisesDetailView: ConfigurableView {
    func buildViewHierarchy() {
        contentView.addSubviews([enterpriseTitleView, descriptionTextView])
        scrollView.addSubviews([contentView])
        addSubviews([scrollView])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            enterpriseTitleView.topAnchor.constraint(equalTo: contentView.topAnchor),
            enterpriseTitleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            enterpriseTitleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            enterpriseTitleView.heightAnchor.constraint(equalToConstant: 120),
            
            descriptionTextView.topAnchor.constraint(equalTo: enterpriseTitleView.bottomAnchor, constant: 16),
            descriptionTextView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            descriptionTextView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            descriptionTextView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
        ])
    }
}
