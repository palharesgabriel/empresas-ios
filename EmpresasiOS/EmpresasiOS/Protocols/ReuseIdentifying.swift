//
//  ReuseIdentifying.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 29/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import Foundation

protocol ReuseIdentifiyng {
    static var reuseIdentifier: String { get }
}

extension ReuseIdentifiyng {
    
    static var reuseIdentifier: String {
        return String(describing: Self.self)
    }
    
}
