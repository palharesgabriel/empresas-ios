//
//  ConfigurableView.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 27/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import Foundation

protocol ConfigurableView {
    func buildViewHierarchy()
    func setupConstraints()
    func setupView()
}

extension ConfigurableView {
    func setupView() {
        buildViewHierarchy()
        setupConstraints()
    }
}
