//
//  UIColor+Extensions.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

extension UIColor {
    static var pinkMainColor = UIColor(red: 224/255, green: 30/255, blue: 105/255, alpha: 1)
    static var purpleMainColor = UIColor(red: 141/255, green: 62/255, blue: 190/255, alpha: 1)
    static var textFieldColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
    static var enterpriseCellBlueColor = UIColor(red: 121/255, green: 187/255, blue: 202/255, alpha: 1)
}
