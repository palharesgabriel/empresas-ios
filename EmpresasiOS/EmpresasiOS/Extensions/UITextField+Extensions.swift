//
//  UITextField+Extensions.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

extension UITextField {
    
    func addEyeButton() -> UIButton {
        let button = UIButton()
        if let image = UIImage(named: "EyeTogle") {
            button.setImage(image, for: .normal)
        }
        button.contentMode = .center
        rightView = button
        rightViewMode = .always
        rightView?.contentMode = .scaleAspectFit
        rightView?.widthAnchor.constraint(equalToConstant: 50).isActive = true
        return button
    }
    
    func setIconOnLeft(with image: UIImage) {
        let imageView = UIImageView()
        imageView.image = image
        leftView = imageView
        leftViewMode = .always
        leftView?.contentMode = .scaleAspectFit
        leftView?.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
}
