//
//  Enterprise.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 29/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import Foundation

struct Result: Decodable {
    let enterprises: [Enterprise]
    
    enum CodingKeys: String, CodingKey {
        case enterprises
    }
}

struct Enterprise: Decodable {
    let enterpriseName: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case enterpriseName = "enterprise_name"
        case description
    }
}
