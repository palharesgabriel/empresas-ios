//
//  APIResponse.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 29/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import Foundation

struct APIResponse: Codable {
    let success: Bool
    let errors: [String]
}
