//
//  UserAuth.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import Foundation

struct UserAuth: Codable {
    let email: String
    let password: String
}
