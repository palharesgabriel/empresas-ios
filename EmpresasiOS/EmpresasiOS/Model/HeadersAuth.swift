//
//  HeadersAuth.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import Foundation

struct HeadersAuth {
    var uid: String?
    var client: String?
    var accessToken: String?
    
    init(response: HTTPURLResponse) {
        if let uid = response.value(forHTTPHeaderField: "uid"), let client = response.value(forHTTPHeaderField: "client"), let accessToken = response.value(forHTTPHeaderField: "access-token") {
            
            self.uid = uid
            self.client = client
            self.accessToken = accessToken
            
        } else {
            self.uid = nil
            self.client = nil
            self.accessToken = nil
        }
    }
    
}
