//
//  LoadingView.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 29/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    
    let activityIndicator = CustomActivityIndicator()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = UIColor.black.withAlphaComponent(0.3)
        setupView()
        
        activityIndicator.startAnimateInternalElipse()
        activityIndicator.startAnimateExternalElipse()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func beSubview(_ view: UIView) {
        view.addSubviews([self])
        
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }

}

extension LoadingView: ConfigurableView {
    func buildViewHierarchy() {
        addSubviews([activityIndicator])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.heightAnchor.constraint(equalToConstant: 80),
            activityIndicator.widthAnchor.constraint(equalToConstant: 80),
        ])
    }
    
}
