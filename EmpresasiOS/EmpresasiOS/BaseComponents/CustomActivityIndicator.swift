//
//  CustomActivityIndicator.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 29/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class CustomActivityIndicator: UIView {
    
    let internalElipse: UIImageView = {
        let imageView = UIImageView()
        if let image = UIImage(named: "InsideElipse") {
            imageView.image = image
        }
        return imageView
    }()
    
    let externalElipse: UIImageView = {
        let imageView = UIImageView()
        if let image = UIImage(named: "OutsideElipse") {
            imageView.image = image
        }
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startAnimateInternalElipse() {
        UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat], animations: {
            self.internalElipse.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat.pi) / 180.0)
            self.internalElipse.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat.pi) / 360.0)
        }, completion: nil)
    }
    
    func startAnimateExternalElipse() {
        UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat], animations: {
            self.externalElipse.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat.pi) / 180.0)
            self.externalElipse.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat.pi) / 360.0)
        }, completion: nil)
    }
    
}

extension CustomActivityIndicator: ConfigurableView {
    func buildViewHierarchy() {
        addSubviews([internalElipse, externalElipse])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            externalElipse.centerXAnchor.constraint(equalTo: centerXAnchor),
            externalElipse.centerYAnchor.constraint(equalTo: centerYAnchor),
            externalElipse.heightAnchor.constraint(equalToConstant: 72),
            externalElipse.widthAnchor.constraint(equalToConstant: 72),
            
            internalElipse.centerYAnchor.constraint(equalTo: centerYAnchor),
            internalElipse.centerXAnchor.constraint(equalTo: centerXAnchor),
            internalElipse.heightAnchor.constraint(equalToConstant: 47),
            internalElipse.widthAnchor.constraint(equalToConstant: 47)
        ])
    }
}
