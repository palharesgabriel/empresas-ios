//
//  EnterpriseTitleView.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 21/05/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class EnterpriseTitleView: UIView {
    
    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        if let image = UIImage(named: "IconTableViewCell") {
            imageView.image = image
        }
        return imageView
    }()
    
    private let enterpriseNameLabel: UILabel = {
        let label = UILabel()
        label.text = "EMPRESA X"
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .enterpriseCellBlueColor
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setContent(enterprise: Enterprise) {
        enterpriseNameLabel.text = enterprise.enterpriseName
    }
    
}

extension EnterpriseTitleView: ConfigurableView {
    func buildViewHierarchy() {
        addSubviews([iconImageView, enterpriseNameLabel])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            iconImageView.topAnchor.constraint(equalTo: topAnchor, constant: 34),
            iconImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            iconImageView.heightAnchor.constraint(equalToConstant: 26),
            iconImageView.widthAnchor.constraint(equalToConstant: 52),
            
            enterpriseNameLabel.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: 4),
            enterpriseNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            enterpriseNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            enterpriseNameLabel.heightAnchor.constraint(equalToConstant: 24),
        ])
    }
}
