//
//  BaseTextField.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class BaseTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 4
        backgroundColor = .textFieldColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addLeftPadding() {
        let viewSpace = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 45))
        self.leftViewMode = .always
        self.leftView = viewSpace
    }
    
    func setWarningBorder(width: CGFloat) {
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.borderWidth = width
    }
    
}
