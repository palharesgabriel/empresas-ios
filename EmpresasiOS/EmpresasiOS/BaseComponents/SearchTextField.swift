//
//  SearchTextField.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 29/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class SearchTextField: BaseTextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        placeholder = "Pesquise por uma empresa"
        clearButtonMode = .whileEditing
        if let image = UIImage(named: "SearchIcon") {
            setIconOnLeft(with: image)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
