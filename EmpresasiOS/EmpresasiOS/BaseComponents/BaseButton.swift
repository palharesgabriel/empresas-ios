//
//  BaseButton.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class BaseButton: UIButton {
    
    convenience init(title: String) {
        self.init(frame: .zero)
        setTitle(title, for: .normal)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 8
        backgroundColor = .pinkMainColor
        setTitleColor(.white, for: .normal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
