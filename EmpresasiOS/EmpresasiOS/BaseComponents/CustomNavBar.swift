//
//  CustomNavBar.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 21/05/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

protocol CustomNavBarDelegate: class {
    func backButtonDidPressed()
}

class CustomNavBar: UIView {
    
    weak var delegate: CustomNavBarDelegate?
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton()
        if let image = UIImage(named: "arrow") {
            button.setImage(image, for: .normal)
        }
        button.addTarget(self, action: #selector(backButtonDidPressed), for: .touchDown)
        return button
    }()
    
    convenience init(delegate: CustomNavBarDelegate) {
        self.init(frame: .zero)
        self.delegate = delegate
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func backButtonDidPressed() {
        delegate?.backButtonDidPressed()
    }
    
    func setTitle(with text: String) {
        titleLabel.text = text
    }
    
}

extension CustomNavBar: ConfigurableView {
    func buildViewHierarchy() {
        addSubviews([titleLabel, backButton])
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.widthAnchor.constraint(equalToConstant: 100),
            titleLabel.heightAnchor.constraint(equalToConstant: 30),
            
            backButton.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            backButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            backButton.widthAnchor.constraint(equalToConstant: 40),
            backButton.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
}
