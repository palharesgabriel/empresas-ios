//
//  GradientView.swift
//  EmpresasiOS
//
//  Created by Gabriel Palhares on 28/03/20.
//  Copyright © 2020 Gabriel Palhares. All rights reserved.
//

import UIKit

class GradientView: UIView {
    
    private var gradient: CAGradientLayer?
    
    var startColor: UIColor? {
        didSet {
            updateGradient()
        }
    }
    
    var endColor: UIColor? {
        didSet {
            updateGradient()
        }
    }
    
    override var frame: CGRect {
        didSet {
            updateGradient()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradient()
    }
    
    convenience init(startColor: UIColor, endColor: UIColor) {
        self.init(frame: .zero)
        self.startColor = startColor
        self.endColor = endColor
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        installGradient()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func installGradient() {
        if let gradient = self.gradient {
            gradient.removeFromSuperlayer()
        }
        let gradient = createGradient()
        self.layer.addSublayer(gradient)
        self.gradient = gradient
    }
    
    private func createGradient() -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        return gradient
    }
    
    private func updateGradient() {
        if let gradient = self.gradient {
            
            let startColor = self.startColor ?? UIColor.clear
            let endColor = self.endColor ?? UIColor.clear
            gradient.colors = [startColor.cgColor, endColor.cgColor]
            
            let start = CGPoint(x: 0, y: 0)
            let end = CGPoint(x: 1, y: 1)
            
            gradient.startPoint = start
            gradient.endPoint = end
            gradient.frame = self.bounds
        }
    }
}
